import requests
import csv

astros = requests.get('http://api.open-notify.org/astros.json')
list_astros = astros.json()['people']
headers_astro = list(list_astros[0].keys())
headers = ('id', headers_astro[0], headers_astro[1])



craft_ISS = []
craft_Tiangong = []

for person in list_astros:
    if person['craft'] == 'ISS':
        craft_ISS.append({'id': len(craft_ISS)+1, 'name': person['name'], 'craft': person['craft']})
    if person['craft'] == 'Tiangong':
        craft_Tiangong.append({'id': len(craft_Tiangong)+1, 'name': person['name'], 'craft': person['craft']})


def write_csv(name, list, headers):
    with open(name+'.csv', 'w') as f:
        writer = csv.DictWriter(f, headers)
        writer.writeheader()
        for row in list:
            writer.writerow(row)


write_csv('ISS', craft_ISS,headers)
write_csv('Tiangong', craft_Tiangong,headers)

